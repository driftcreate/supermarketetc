<footer>
	<div class="topFooter">
		<div class="container">
			<div class="row">
				<div class="col-xs-3 17">
					<?php the_field('footer_text', 'option'); ?>
				</div>
				<div class="col-xs-3">
					<p>
						<strong>
							Visit Us:
						</strong>
					</p>
					<?php the_field('address', 'option'); ?>
				</div>
				<div class="col-xs-3">
					<p>
						<strong>
							Hours of Operation:
						</strong>
					</p>
					<?php the_field('hours_of_operation', 'option'); ?>
				</div>
				<div class="col-xs-3">
					<p>
						<strong>
							Contact Us:
						</strong>
					</p>
					<p>
						<?php the_field('phone_number', 'option'); ?>
						<br>
						<?php the_field('email', 'option'); ?>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="botFooter">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p>
						Copyright ©2018. Site by <a href="https://driftingcreatives.com" target="_blank">Drift.</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</footer>

		<?php wp_footer(); ?>

	    <script>
			$('.productSlider').slick({
				infinite: true,
				slidesToShow: 5,
				slidesToScroll: 1,
				responsive: [
			    {
			      breakpoint: 768,
			      settings: {
			        slidesToShow: 4,
			        slidesToScroll: 1,
			        infinite: true
			      }
			    },
			    {
			      breakpoint: 600,
			      settings: {
			        slidesToShow: 2,
			        slidesToScroll: 2
			      }
			    },
			    {
			      breakpoint: 400,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    }
			    // You can unslick at a given breakpoint now by adding:
			    // settings: "unslick"
			    // instead of a settings object
			  ]
			});
			$('.historySlider').slick({
				infinite: true,
				slidesToShow: 1,
				slidesToScroll: 1
			});
			$(".hamover").click( function() {
				$(this).parent().find(".hamburger").toggleClass("hambx");
				$("div.mobile-menu").slideToggle();
				$("header").toggleClass("active");
				$("body, html").toggleClass("no-scroll");
			});
	    </script>

	    <!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	
	</body>
</html>