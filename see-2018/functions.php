<?php
// basic stuff //////////////////////////////////////////////
register_nav_menu( 'primary', 'Primary Menu' );
add_theme_support( 'post-thumbnails' );
// /basic stuff /////////////////////////////////////////////



// styles/scripts ///////////////////////////////////////////
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
   wp_deregister_script('jquery');
   wp_register_script('jquery', "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js", false, null);
   wp_enqueue_script('jquery');
}

function add_theme_scripts() {
    wp_enqueue_style( 'style', get_stylesheet_uri() );
    wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css', false, '1.0.0', 'all' );
    wp_enqueue_style( 'robotoFont', 'https://fonts.googleapis.com/css?family=Roboto:300,400,400i,700,900', false, '1.0.0', 'all' );
    wp_enqueue_style( 'main', get_template_directory_uri() . '/css/main.css', false, '1.0.0', 'all' );
    wp_enqueue_style( 'slick', get_template_directory_uri() . '/css/slick.css', false, '1.0.0', 'all' );

    wp_enqueue_script( 'slick', get_stylesheet_directory_uri() . '/js/slick.min.js', array ( 'jquery' ), 1.1, true );
    wp_enqueue_script( 'hoverintent', get_stylesheet_directory_uri() . '/js/hoverintent.min.js', array ( 'jquery' ), 1.1, true );
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );
// /styles/scripts //////////////////////////////////////////



// options page /////////////////////////////////////////////
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}
// /options page ////////////////////////////////////////////



// waldo ////////////////////////////////////////////////////
include( 'waldo-master/waldo.php' );

function wpse87681_enqueue_custom_stylesheets() {
    if ( ! is_admin() ) {
        wp_enqueue_style( 'mytheme-custom', get_template_directory_uri() . '/waldo.css' );
    }
}
add_action( 'wp_enqueue_scripts', 'wpse87681_enqueue_custom_stylesheets', 11 );

add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
    add_image_size('xlarge', 1400);
    add_image_size('xxlarge', 1800);
    add_image_size('xxxlarge', 2200);
    add_image_size('xxxxlarge', 2500);
}
// /waldo ///////////////////////////////////////////////////

// generate post-type (store type)
// Register Custom Post Type
function storeTypePostType() {

	$labels = array(
		'name'                  => 'Store Types',
		'singular_name'         => 'Store Type',
		'menu_name'             => 'Store Types',
		'name_admin_bar'        => 'Store Type',
		'archives'              => 'Item Archives',
		'attributes'            => 'Item Attributes',
		'parent_item_colon'     => 'Parent Item:',
		'all_items'             => 'All Items',
		'add_new_item'          => 'Add New Item',
		'add_new'               => 'Add New',
		'new_item'              => 'New Item',
		'edit_item'             => 'Edit Item',
		'update_item'           => 'Update Item',
		'view_item'             => 'View Item',
		'view_items'            => 'View Items',
		'search_items'          => 'Search Item',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'insert_into_item'      => 'Insert into item',
		'uploaded_to_this_item' => 'Uploaded to this item',
		'items_list'            => 'Items list',
		'items_list_navigation' => 'Items list navigation',
		'filter_items_list'     => 'Filter items list',
	);
	$rewrite = array(
		'slug'                  => 'store-type',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => 'Store Type',
		'description'           => 'Post Type Description',
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'				=> $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'storePostType', $args );

}
add_action( 'init', 'storeTypePostType', 0 );

// acf image helpers ////////////////////////////////////////////////////
function ar_responsive_image($image_id,$image_size,$max_width){
    // check the image ID is not blank
    if($image_id != '') {
        // set the default src image size
        $image_src = wp_get_attachment_image_url( $image_id, $image_size );
        // set the srcset with various image sizes
        $image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );
        // generate the markup for the responsive image
        echo 'src="'.$image_src.'" srcset="'.$image_srcset.'" sizes="(max-width: '.$max_width.') 100vw, '.$max_width.'"';
    }
}

function acf_srcset($image_id,$image_size,$max_width){
  // check the image ID is not blank
  if($image_id != '') {
    // set the default src image size
    $image_src = wp_get_attachment_image_url( $image_id, 'xxlarge' );
    // set the srcset with various image sizes
    $image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );
    // generate the markup for the responsive image
    echo 'src="'.$image_src.'" srcset="'.$image_srcset.'" sizes="(max-width: '.$max_width.') 100vw, '.$max_width.'"';
  }
}
// /acf image helpers ///////////////////////////////////////////////////
?>