<?php get_header(); ?>

<main class="posts">

	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-8">
				
					<?php 
					// args
					$args = array(
						'post_type'		=> 'post',
						'posts_per_page'=> 1
					);

					// query
					$the_query = new WP_Query( $args );
					?>

					<?php if( $the_query->have_posts() ): ?>
						<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>

							<?php
							$thumb_id = get_post_thumbnail_id();
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
							$thumb_url = $thumb_url_array[0];
							?>

							<a href="<?php the_permalink(); ?>">
								<?php the_title('<h1 class="posts-title">', '</h1>'); ?>
								<h2 class="the-date"><?php echo the_time('F j, Y'); ?></h2>
								<div class="featured-post" style="background:url(<?php echo $thumb_url; ?>) center no-repeat; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
									<div class="the-excerpt"><p><?php echo excerpt(45); ?></p></div>
								</div>
							</a>

						<?php endwhile; ?>
					<?php endif; ?>

					<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?><div class="clear"></div>



					<?php
					$current_page = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$max_entries_per_page = 4;
					// args
					$args = array(
						'post_type'		=> 'post',
						'posts_per_page'=> 4,
						'offset'		=> 1,
						'paged'			=> $current_page
					);

					// query
					$the_query = new WP_Query( $args );
					?>
										
					<?php if( $the_query->have_posts() ): ?>
						<ul class="not-featured-posts">
						<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>

							<?php
							$thumb_id2 = get_post_thumbnail_id();
							$thumb_url_array2 = wp_get_attachment_image_src($thumb_id2, 'thumbnail-size', true);
							$thumb_url2 = $thumb_url_array2[0];
							?>

							<li>
								<a href="<?php the_permalink(); ?>">
									<?php the_title('<h1 class="posts-title">', '</h1>'); ?>
									<h2 class="the-date"><?php echo the_time('F j, Y'); ?></h2>
									<div class="not-featured-post" style="background:url(<?php echo $thumb_url2; ?>) center no-repeat; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
										<div class="the-excerpt"><p><?php echo excerpt(15); ?></p></div>
									</div>
								</a>
							</li>
						<?php endwhile; ?>
						</ul>
					<?php endif; ?>
					<div class="clear"></div>
										
					<nav class="pagination"><?php wp_pagination(); ?></nav>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</section>

</main>

<?php get_footer(); ?>