<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">

			<?php
			if ( have_posts() ) {
			    while ( have_posts() ) {
			        the_post();
			        the_title( '<h1 style="display:none;">', '</h1>' );
			        the_content();
			    }
			}
			wp_footer();
			?>

		</div>
	</div>
</div>

<?php get_footer(); ?>