<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



<section class="s1-single-type">
	<div class="container">
		<div class="row">
			<div class="col-md-8 mobile">
				<?php
				    $image = get_field('image');
				    if( !empty( $image ) ) {?>
				      <img <?php acf_srcset( $image['id'], 'large', '100vw' ); ?> alt="<?php echo $image['alt']; ?>" />
				    <?php
				    }
				?>
			</div>
			<div class="col-md-4">
				<div class="text">
					<?php the_title('<h1 class="article-title">', '</h1>'); ?>
					<a href="<?php the_field('link'); ?>"><div class="blue-button">Shop Now</div></a>
				</div>
			</div>
			<div class="col-md-6 col-md-offset-5 no-mobile">
				<?php
				    $image = get_field('image');
				    if( !empty( $image ) ) {?>
				      <img <?php acf_srcset( $image['id'], 'large', '100vw' ); ?> alt="<?php echo $image['alt']; ?>" />
				    <?php
				    }
				?>
			</div>
		</div>
	</div>
</section>

<section class="s2-single-type">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<?php the_field('text'); ?>
			</div>
			<div class="col-md-4">
				<div class="grey-box">
					<p>Don’t see what you’re looking for? We’re happy to help! Give us a call at <?php the_field('phone_number', 'option'); ?></p>
				</div>
			</div>
		</div>
	</div>
</section>



<?php endwhile; else: ?>

<?php endif; ?>

<?php get_footer(); ?>