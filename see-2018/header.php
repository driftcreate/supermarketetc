<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <title>
	    	<?php
				/*
				 * Print the <title> tag based on what is being viewed.
				 */
				global $page, $paged;

				wp_title( '|', true, 'right' );

				// Add the blog name.
				bloginfo( 'name' );
			?>
		</title>

	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	    	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	    	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	    <?php wp_head(); ?>
	</head>
	<body>
		
		<header>
			<div class="container">
				<div class="row">
					<div class="col-sm-4 no-mobile">
						<a href="<?php bloginfo('url'); ?>"><img src="<?php the_field('logo', 'option'); ?>" alt=""></a>
					</div>
					<div class="col-sm-8 no-mobile">
						<div class="aboveNav">
							<div class="espanol">
								<p>Español</p>
							</div>
							<div class="phone">
								<p>Give us a call at <?php the_field('phone_number', 'option'); ?></p>
							</div>
							<div class="clear"></div>
						</div>
						<nav>
							<?php wp_nav_menu(array('theme_location'=>'primary','container'=>'div','container_id'=>'nav-menu')); ?>
						</nav>
					</div>

					<div class="col-xs-6 mobile">
						<a href="<?php bloginfo('url'); ?>"><img src="<?php the_field('logo', 'option'); ?>" alt=""></a>
					</div>
					<div class="col-xs-6 mobile">
						<div class="mobile mob-men">
							<div class="menu-container">
								<div class="hamover"></div>
								<div class="hamburger"></div>
							</div>
						</div>
						<div class="espanol">
							<p>Español</p>
						</div>
					</div>
				</div>
			</div>
		</header>

		<div class="mobile-menu">
			<?php wp_nav_menu(array('theme_location'=>'primary','container'=>'div','container_id'=>'nav-menu')); ?>
			<div class="bottom">
				<p>Give us a call at <?php the_field('phone_number', 'option'); ?></p>
			</div>
		</div>

