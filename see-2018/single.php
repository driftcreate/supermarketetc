<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
$thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
$thumb_url = $thumb_url_array[0];
?>

<main class="post">

	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<?php the_title('<h1 class="article-title">', '</h1>'); ?>
					<h2 class="the-date"><?php echo the_time('F j, Y'); ?></h2>

					<div class="centered">
						<img src="<?php echo $thumb_url; ?>" />
					</div>

					<?php the_content(); ?>
				</div>

				<div class="col-md-3">
					<div class="article-sidebar">
						<li>
							<h1>Search</h1>
							<form action="<?php bloginfo('home'); ?>/" method="get" id="searchform">
								<input type="text" name="s" value="<?php the_search_query(); ?>" placeholder="search">
							</form>
						</li>
						<?php dynamic_sidebar( 'sidebar-1' ); ?>
					</div>
				</div>

			</div>
		</div>
	</section>

</main>

<?php endwhile; else: ?>

<?php endif; ?>

<?php get_footer(); ?>