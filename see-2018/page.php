<?php get_header(); ?>

<main class="default-page">

	<section class="default-page">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php
					if ( have_posts() ) {
					    while ( have_posts() ) {
					        the_post();
					        the_title( '<h1 class="title">', '</h1>' );
					        the_content();
					    }
					}
					wp_footer();
					?>
				</div>
			</div>
		</div>
	</section>

</main>

<?php get_footer(); ?>