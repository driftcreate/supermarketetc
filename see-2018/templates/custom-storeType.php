<?php
/*
Template Name: StoreType
*/
?>

<?php get_header(); ?>
<section class="s1-storeType">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>
                    <?php the_field('s1_title'); ?>
                </h1>
                <hr>
                <p>
                    <?php the_field('s1_text'); ?>
                </p>
            </div>
        </div>
    </div>
</section>
<section class="s2-storeType">
    <div class="container">
        <div class="row">
            <?php echo do_shortcode('[facetwp template="store_types"]'); ?>
            <div class="col-md-4">
                <div class="finalTile" style="background:url(<?php the_field('s2_bg_image'); ?>) center no-repeat; background-size: cover;">
                    <div class="inside">
                        <h1><?php the_field('s2_text'); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="greyBar">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <p>
                        <?php the_field('bottom_text'); ?>
                    </p>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>