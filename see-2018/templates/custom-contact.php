<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>

<?php
    // section 1
    $image = get_field('s1_bgImage');
    $waldo_class = 's1-contact';
	$waldo_styles = $waldo->waldoStylesArray($image, $waldo_class, $waldo_styles, $waldo_class);

?>

<section class="s1-contact">
    <div class="container"><div class="row"><div class="col-md-12"></div></div></div>
</section>
<section class="s2-contact">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>
                    <?php the_field('s2_title'); ?>
                </h1>
                <hr>
                <p>
                    <?php the_field('s2_text'); ?>
                </p>
            </div>
        </div>
    </div>
    <div class="blueBar">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <p>
                        <?php the_field('s2_botText'); ?>
                    </p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="s3-contact">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="visitUs">
                    <h3>Visit Us</h3>
                    <hr>
                    <?php the_field('address', 'option'); ?>
                </div>
                <div class="hours">
                    <h3>Hours of Operation</h3>
                    <hr>
                    <?php the_field('hours_of_operation', 'option'); ?>
                </div>
                <div class="contactUs">
                    <h3>Contact Us</h3>
                    <hr>
                    <p>
						<?php the_field('phone_number', 'option'); ?>
						<br>
						<?php the_field('email', 'option'); ?>
					</p>
                </div>
                <div class="clear"></div>
            </div>
            <div class="col-md-8">
                <?php echo do_shortcode('[contact-form-7 id="101" title="Contact form 1"]'); ?>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>