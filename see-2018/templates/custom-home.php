<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>

<?php
    // section 2
    $image = get_field('s2_bgImage');
    $waldo_class = 's2-home';
	$waldo_styles = $waldo->waldoStylesArray($image, $waldo_class, $waldo_styles, $waldo_class);

	// section 5
    $image = get_field('s5_backgroundImage');
    $waldo_class = 's5-home';
	$waldo_styles = $waldo->waldoStylesArray($image, $waldo_class, $waldo_styles, $waldo_class);
?>


<section class="s1-home">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<?php the_field('s1_text'); ?>
				<?php 

				$link = get_field('s1_link');

				if( $link ): ?>
					
					<a href="<?php echo $link['url']; ?>"><div class="blue-button">View Our Products</div></a>

				<?php endif; ?>
			</div>
			<div class="col-md-6">
			<?php
				$image = get_field('s1_image');
				if( !empty( $image ) ) {?>
					<img class="overhangImage" <?php acf_srcset( $image['id'], 'large', '42vw' ); ?> alt="<?php echo $image['alt']; ?>" />
				<?php
				}
			?>
			</div>
		</div>
	</div>
</section>
<section class="s2-home">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">	
				<?php the_field('s2_text'); ?>
			</div>
		</div>
	</div>
</section>
<section class="s3-home">
	<?php if( have_rows('s3_left') ): while ( have_rows('s3_left') ) : the_row(); ?>
		<div class="left" style="background: url(<?php the_sub_field('background_image'); ?>);">
			<img class="logo" src="<?php the_sub_field('logo'); ?>" alt="">
			<div class="inside">
				<?php the_sub_field('text'); ?>
				
				<?php $link = get_sub_field('link'); if( $link ): ?>	
					<a href="<?php echo $link['url']; ?>"><div class="blue-button">View Products</div></a>
				<?php endif; ?>
			</div>
		</div>
	<?php endwhile; endif; ?>
	<?php if( have_rows('s3_right') ): while ( have_rows('s3_right') ) : the_row(); ?>
		<div class="right" style="background: url(<?php the_sub_field('background_image'); ?>);">
			<div class="inside">
				<?php the_sub_field('text'); ?>
				
				<?php $link = get_sub_field('link'); if( $link ): ?>	
					<a href="<?php echo $link['url']; ?>"><div class="blue-button">View Products</div></a>
				<?php endif; ?>
			</div>
		</div>
	<?php endwhile; endif; ?>
	<div class="clear"></div>
</section>
<section class="s4-home">
	<div class="container">
		<div class="row">
			<div class="col-xs-10 col-xs-offset-1">
				<h1>FIND PRODUCTS FOR YOUR MARKET</h1>
				<div class="productSlider">
				<?php if( have_rows('s4_products') ): while ( have_rows('s4_products') ) : the_row(); ?>
					<div>
						<a href="<?php the_sub_field('link'); ?>">
							<img class="icon" src="<?php the_sub_field('icon'); ?>" alt="">
							<p><?php the_sub_field('title'); ?></p>
						</a>
					</div>
				<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="greyBar">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php the_field('s4_text'); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="s5-home">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<?php the_field('s5_text'); ?>
				<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Contact' ) ) ); ?>"><div class="blue-button">Contact Us</div></a>
			</div>
		</div>
	</div>
</section>
<section class="s6-home">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h2>Stay Updated</h2>
				<hr>
				<p>Stay up to date with all our products by signing up for our newsletter.</p>
			</div>
			<div class="col-md-9">
				<form>
					<label for="email">
						<p>Email:</p>
						<input type="text" name="email">
					</label>
					<label for="name">
						<p>Name:</p>
						<input type="text" name="name">
					</label>
					<input class="blue-button" type="submit">
				</form>
			</div>
		</div>
	</div>
</section>


<?php get_footer(); ?>