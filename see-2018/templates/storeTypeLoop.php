<?php while ( have_posts() ): the_post(); ?>
<div class="col-md-4 tile">
    <a href="<?php the_permalink(); ?>">
        <div class="storeTypeContainer">
            <div class="inside">
                <img src="<?php the_field('icon'); ?>">
                <p>
                    <?php the_title(); ?>
                </p>
            </div>
        </div>
        <div class="blueBar1">
            <p>View Products</p>
        </div>
    </a>
</div>
<?php endwhile; ?>