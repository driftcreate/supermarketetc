<?php
/*
Template Name: About
*/
?>

<?php get_header(); ?>

<?php
    // section 1
    $image = get_field('s1_about');
    $waldo_class = 's1-about';
    $waldo_styles = $waldo->waldoStylesArray($image, $waldo_class, $waldo_styles, $waldo_class);
    
    // section 4
    $image = get_field('s4_bgImage');
    $waldo_class = 's4-about';
	$waldo_styles = $waldo->waldoStylesArray($image, $waldo_class, $waldo_styles, $waldo_class);

?>

<section class="s1-about">
    <div class="container"><div class="row"><div class="col-md-12"></div></div></div>
</section>

<section class="s2-about">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="left">
                    <h1><?php the_field('s2_title'); ?></h1>
                    <hr>
                    <p><?php the_field('s2_text'); ?></p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="right">
                    <img class="logo" src="<?php the_field('s2_logo'); ?>" alt="">
                    <p><?php the_field('s2_text2'); ?></p>
                    <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Products' ) ) ); ?>"><div class="blue-button">View Products</div></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="s3-about">
    <div class="top">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-3 col-md-6">
                    <h1><?php the_field('s3_title'); ?></h1>
                    <hr>
                </div>
            </div>
        </div>
    </div>
    <div class="container bottom">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-10">
                <div class="historySlider">
                    <?php if( have_rows('s3_bottom') ): while ( have_rows('s3_bottom') ) : the_row(); ?>
                        <div>
                            <img class="historyImage" src="<?php the_sub_field('image'); ?>" alt="">
                            <!--<p><?php the_sub_field('text'); ?></p>-->
                        </div>
                    <?php endwhile; endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="s4-about">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1><?php the_field('s4_text'); ?></h1>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
