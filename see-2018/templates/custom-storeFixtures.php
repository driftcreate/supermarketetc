<?php
/*
Template Name: Store Fixtures
*/
?>

<?php get_header(); ?>
<section class="s1-storeFixture">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>
                    <?php the_field('s1_title'); ?>
                </h1>
                <hr>
                <p>
                    <?php the_field('s1_text'); ?>
                </p>
            </div>
        </div>
    </div>
</section>
<section class="s2-storeFixture">
    <div class="container">
        <div class="row">
            <?php if( have_rows('store_fixtures') ): while ( have_rows('store_fixtures') ) : the_row(); ?>
                <div class="col-md-4">
                    <a href="<?php the_sub_field('link'); ?>">
                        <div class="storeFixtureContainer">
                            <div class="inside">
                                <img class="image" src="<?php the_sub_field('image'); ?>" alt="">
                                <p><?php the_sub_field('title_description'); ?></p>
                            </div>
                        </div>
                        <div class="blueBar1">Shop Now</div>
                    </a>
                </div>
            <?php endwhile; endif; ?>
            <div class="col-md-4">
                <div class="finalTile">
                    <div class="inside">
                        <h1><?php the_field('s2_text'); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="greyBar">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <p>
                        <?php the_field('gray_bar'); ?>
                    </p>
				</div>
			</div>
		</div>
    </div>
</section>
<section class="s3-storeFixture">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h1><?php the_field('quote_text'); ?></h1>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php echo do_shortcode('[contact-form-7 id="228" title="Store Fixtures"]'); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>