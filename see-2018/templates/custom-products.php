<?php
/*
Template Name: Products
*/
?>

<?php get_header(); ?>
<section class="s1-products">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>
                    <?php the_field('s1_title'); ?>
                </h1>
                <hr>
                <p>
                    <?php the_field('s1_text'); ?>
                </p>
            </div>
        </div>
    </div>
</section>
<section class="s2-products">
    <div class="container">
        <div class="row">
            <?php if( have_rows('products') ): while ( have_rows('products') ) : the_row(); ?>
                <div class="col-md-4">
                    <a href="<?php the_sub_field('link'); ?>">
                        <div class="storeFixtureContainer">
                            <div class="inside">
                                <img class="image" src="<?php the_sub_field('image'); ?>" alt="">
                                <p><?php the_sub_field('title'); ?></p>
                            </div>
                        </div>
                        <div class="blueBar1">Shop Now</div>
                    </a>
                </div>
            <?php endwhile; endif; ?>
            <div class="col-md-4">
                <div class="finalTile" style="background:url(<?php the_field('bg_image'); ?>) center no-repeat; background-size: cover;">
                    <div class="inside">
                        <h1><?php the_field('text'); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="greyBar">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <p>
                        <?php the_field('bottomBarText'); ?>
                    </p>
				</div>
			</div>
		</div>
    </div>
</section>
<?php get_footer(); ?>