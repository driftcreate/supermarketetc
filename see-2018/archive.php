<?php get_header(); ?>

<main class="posts">

	<section class="blog-post-content">
		<div class="container">
			<div class="row">

				<div class="col-md-9">
					<div class="super-article">
						<div class="inside-left-super">
										
							<?php if (have_posts()) : ?>
								<ul class="not-featured-posts">
									<?php while (have_posts()) : the_post(); ?>

										<?php
										$thumb_id2 = get_post_thumbnail_id();
										$thumb_url_array2 = wp_get_attachment_image_src($thumb_id2, 'thumbnail-size', true);
										$thumb_url2 = $thumb_url_array2[0];
										?>

										<li>
											<a href="<?php the_permalink(); ?>">
												<?php the_title('<h1 class="posts-title">', '</h1>'); ?>
												<h2 class="the-date"><?php echo the_time('F j, Y'); ?></h2>
												<div class="not-featured-post" style="background:url(<?php echo $thumb_url2; ?>) center no-repeat; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
													<div class="the-excerpt"><p><?php echo excerpt(15); ?></p></div>
												</div>
											</a>
										</li>
									<?php endwhile; ?>
								</ul>
							<?php endif; ?>
							<div class="clear"></div>
										
							<nav class="pagination"><?php wp_pagination(); ?></nav>
							<div class="clear"></div>

						</div><div class="clear"></div>
					</div>
				</div>

				<div class="col-md-3">
					<div class="article-sidebar">
						<li>
							<h1>Search</h1>
							<form action="<?php bloginfo('home'); ?>/" method="get" id="searchform">
								<input type="text" name="s" value="<?php the_search_query(); ?>" placeholder="search">
							</form>
						</li>
						<?php dynamic_sidebar( 'sidebar-1' ); ?>
					</div>
				</div>

			</div>
		</div>
	</section>

</main>

<?php get_footer(); ?>